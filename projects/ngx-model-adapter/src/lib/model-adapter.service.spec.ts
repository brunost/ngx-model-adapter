import { TestBed, async } from "@angular/core/testing";

import { ModelAdapterService } from "./model-adapter.service";
import { IsString, IsNumber, ValidateNested } from "class-validator";
import { Expose, Transform, Type } from "class-transformer";
import { of } from "rxjs";

describe("ModelAdapterService", () => {
  let modelAdapter: ModelAdapterService;

  class Model {
    @IsString() str: string;
  }

  class ModelExpose {
    @IsString()
    @Expose({ name: "exposedStr" })
    public str: string;
  }

  class ModelTransformToClass {
    @IsNumber()
    @Transform(v => Number.parseInt(v, 10), { toClassOnly: true })
    public num: number;
  }

  class ModelTransformToPlain {
    @IsNumber()
    @Transform(v => String(v), { toPlainOnly: true })
    public num: number;
  }

  class ModelNested {
    @Type(() => Model) @ValidateNested()
    public nested: Model;
    @Type(() => Model) @ValidateNested({ each: true })
    public nestedArray: Model[];
  }

  beforeEach(() => {
    TestBed.configureTestingModule({});
    modelAdapter = TestBed.get(ModelAdapterService);
  });

  it("should be created", () => {
    expect(modelAdapter).toBeTruthy();
  });

  describe("#to", () => {

    describe("single", () => {

      it("instantiates model class from plain object", async(() => {
        const plain = { str: "banana" };
        of(plain).pipe(modelAdapter.toModel(Model)).subscribe(res => {
          expect(res instanceof Model).toBeTruthy();
          expect(res.str).toBe("banana");
        });
      }));

      it("throws error if validation fails", async(() => {
        const plain = { str: 123 };
        of(plain).pipe(modelAdapter.toModel(Model)).subscribe(
          () => expect("success").toBe("error"),
          errors => expect(errors).toBeTruthy()
        );
      }));

      it("maps exposed properties correctly", async(() => {
        const plain = { exposedStr: "banana" };
        of(plain).pipe(modelAdapter.toModel(ModelExpose)).subscribe(res => {
          expect(res instanceof ModelExpose).toBeTruthy();
          expect(res.str === "banana").toBeTruthy();
        });
      }));

      it("maps transformed (to class only) properties correctly", async(() => {
        const plain = { num: "123" };
        of(plain).pipe(modelAdapter.toModel(ModelTransformToClass)).subscribe(res => {
          expect(res instanceof ModelTransformToClass).toBeTruthy();
          expect(typeof res.num === "number").toBeTruthy();
        });
      }));

      it("maps nested model properties correctly", async(() => {
        const plain = {
          nested: { str: "banana" },
          nestedArray: [
            { str: "orange" },
            { str: "apple" }
          ]
        };
        of(plain).pipe(modelAdapter.toModel(ModelNested)).subscribe(res => {
          expect(res instanceof ModelNested).toBeTruthy();
          expect(res.nested instanceof Model).toBeTruthy();
          expect(res.nested.str).toBe("banana");
          expect(Array.isArray(res.nestedArray)).toBeTruthy();
          const m1 = res.nestedArray[0];
          expect(m1 instanceof Model).toBeTruthy();
          expect(m1.str).toBe("orange");
          const m2 = res.nestedArray[1];
          expect(m2 instanceof Model).toBeTruthy();
          expect(m2.str).toBe("apple");
        });
      }));

    });

    describe("array", () => {

      it("instantiates model classes from array of plain objects", async(() => {
        const plains = [
          { str: "banana" },
          { str: "orange" }
        ];
        of(plains).pipe(modelAdapter.toModel([Model])).subscribe(res => {
          expect(Array.isArray(res)).toBeTruthy();
          const m1 = res[0];
          expect(m1 instanceof Model).toBeTruthy();
          expect(m1.str).toBe("banana");
          const m2 = res[1];
          expect(m2 instanceof Model).toBeTruthy();
          expect(m2.str).toBe("orange");
        });
      }));

      it("throws error if plain is not array", async(() => {
        const plain = { str: "banana" };
        of(plain).pipe(modelAdapter.toModel([Model])).subscribe(
          () => expect("success").toBe("error"),
          errors => expect(errors).toBeTruthy()
        );
      }));

      it("throws error if validation fails for any item in array", async(() => {
        const plains = [
          { str: "banana" },
          { str: 123 }
        ];
        of(plains).pipe(modelAdapter.toModel([Model])).subscribe(
          () => expect("success").toBe("error"),
          errors => expect(errors).toBeTruthy()
        );
      }));

      it("maps exposed properties correcly", async(() => {
        const plains = [{ exposedStr: "banana" }];
        of(plains).pipe(modelAdapter.toModel([ModelExpose])).subscribe(res => {
          expect(Array.isArray(res)).toBeTruthy();
          const m1 = res[0];
          expect(m1 instanceof ModelExpose).toBeTruthy();
          expect(m1.str === "banana").toBeTruthy();
        });
      }));

      it("maps transformed (to class only) properties correcly", async(() => {
        const plains = [{ num: "123" }];
        of(plains).pipe(modelAdapter.toModel([ModelTransformToClass])).subscribe(res => {
          expect(Array.isArray(res)).toBeTruthy();
          const m1 = res[0];
          expect(m1 instanceof ModelTransformToClass).toBeTruthy();
          expect(typeof m1.num === "number").toBeTruthy();
        });
      }));

      it("maps nested model properties correctly", async(() => {
        const plain = [
          {
            nested: { str: "banana" },
            nestedArray: [
              { str: "orange" },
              { str: "apple" }
            ]
          }
        ];
        of(plain).pipe(modelAdapter.toModel([ModelNested])).subscribe(res => {
          expect(Array.isArray(res)).toBeTruthy();
          const m1 = res[0];
          expect(m1 instanceof ModelNested).toBeTruthy();
          expect(m1.nested instanceof Model).toBeTruthy();
          expect(m1.nested.str).toBe("banana");
          expect(Array.isArray(m1.nestedArray)).toBeTruthy();
          const nm1 = m1.nestedArray[0];
          expect(nm1 instanceof Model).toBeTruthy();
          expect(nm1.str).toBe("orange");
          const nm2 = m1.nestedArray[1];
          expect(nm2 instanceof Model).toBeTruthy();
          expect(nm2.str).toBe("apple");
        });
      }));

    });

  });

  describe("#from", () => {

    it("transforms model class to plain object", async(() => {
      const plain = { str: "banana" };
      modelAdapter.fromModel(Model, plain).subscribe(res => {
        expect(typeof res).toBe("object");
        expect(res.str).toBe("banana");
      });
    }));

    it("throws error if validation fails", async(() => {
      const plain = {};
      modelAdapter.fromModel(Model, plain as any).subscribe(
        () => expect("success").toBe("error"),
        errors => expect(errors).toBeTruthy()
      );
    }));

    it("mapping exposed properties (with workaround) works", async(() => {
      const plain = new ModelExpose(); // For now, models with exposed properties must be instantiated manually
      plain.str = "banana";
      modelAdapter.fromModel(ModelExpose, plain).subscribe(
        res => {
          expect(res instanceof ModelExpose).toBeFalsy();
          expect(res.exposedStr).toBe("banana");
        },
        err => expect(err).toBeFalsy()
      );
    }));

    it("maps transformed (to plain only) properties correctly", async(() => {
      const plain = { num: 123 };
      modelAdapter.fromModel(ModelTransformToPlain, plain).subscribe(res => {
        expect(res instanceof ModelTransformToPlain).toBeFalsy();
        expect(res.num).toBe("123");
      });
    }));

    it("maps nested model properties correctly", async(() => {
      const plain = {
        nested: { str: "banana" },
        nestedArray: [
          { str: "orange" }
        ]
      };
      modelAdapter.fromModel(ModelNested, plain).subscribe(res => {
        expect(res instanceof ModelNested).toBeFalsy();
        expect(res).toEqual({
          nested: { str: "banana" },
          nestedArray: [{ str: "orange" }]
        });
      });
    }));

  });

});
