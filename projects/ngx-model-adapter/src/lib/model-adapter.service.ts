import { Injectable, InjectionToken, Inject } from "@angular/core";
import { OperatorFunction, from, throwError, Observable, of, forkJoin } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { validate, ValidatorOptions } from "class-validator";
import { plainToClass, classToPlain, ClassTransformOptions } from "class-transformer";

export interface ModelAdapterOptions {
  validator?: ValidatorOptions;
  transformer?: ClassTransformOptions;
}

/**
 * Model Adapter Options
 *
 * Usage:
 * ```ts
 *  @NgModule({
 *    providers: [
 *      {
 *        provide: MODEL_ADAPTER_OPTIONS,
 *        useValue: {
 *          validator: {}, // ValidatorOptions (class-validator)
 *          transformer: {} // ClassTransformOptions (class-transformer)
 *        }
 *      }
 *    ]
 *  })
 *  export class AppModule {}
 * ```
 */
export const MODEL_ADAPTER_OPTIONS = new InjectionToken<ModelAdapterOptions>("ModelAdapterOptions", {
  providedIn: "root",
  factory: () => ({})
});

// @dynamic
@Injectable({ providedIn: "root" })
export class ModelAdapterService {

  constructor(@Inject(MODEL_ADAPTER_OPTIONS) private readonly options: ModelAdapterOptions) {}

  /**
   * Map a plain object to a model and validate it.
   *
   * Usage:
   * ```ts
   *  httpClient.get(url).pipe(
   *    modelAdapter.toModel(Model)
   *  ).subscribe(
   *    (model: Model) => console.log(model),
   *    (err: HttpErrorResponse|ValidationError[]) => console.error(err)
   *  );
   * ```
   */
  public toModel<T>(modelType: new () => T, optionsOverride?: ModelAdapterOptions): OperatorFunction<unknown, T>;

  /**
   * Map an array of plain objects to an array of validated models
   *
   * Usage:
   * ```ts
   *  httpClient.get(url).pipe(
   *    modelAdapter.toModel([Model]) // note the brackets
   *  ).subscribe(
   *    (models: Model[]) => console.log(models),
   *    (err: HttpErrorResponse|Array<ValidationError[]>) => console.error(err)
   *  );
   * ```
   */
  public toModel<T>(modelType: [new () => T], optionsOverride?: ModelAdapterOptions): OperatorFunction<unknown, T[]>;

  public toModel<T>(modelType: (new () => T)|[(new () => T)], optionsOverride?: ModelAdapterOptions): OperatorFunction<unknown, T|T[]> {
    const opts = this.overrideOptions(optionsOverride);
    return switchMap<unknown, T|T[]>(value => {
      if (Array.isArray(modelType)) {
        return this.toModelArray(modelType[0], value, opts);
      } else {
        return this.toModelSingle(modelType, value, opts);
      }
    });
  }

  // Mapping and validating a single model
  private toModelSingle<T>(modelType: new () => T, value: unknown, opts: ModelAdapterOptions): Observable<T> {
    if (typeof value !== "object") return throwError(new Error("value is not an object"));
    const model = plainToClass(modelType, value, opts.transformer);
    return from(validate(model, opts.validator)).pipe(map(errors => {
      if (errors.length > 0) throw errors;
      return model;
    }));
  }

  // Mapping and validating an array of models
  private toModelArray<T>(modelType: new () => T, value: unknown, opts: ModelAdapterOptions): Observable<T[]> {
    if (!Array.isArray(value)) return throwError(new Error("value is not an array"));
    const models: T[] = [];
    for (let i = 0; i < value.length; i++) {
      const plain: unknown = value[i];
      if (typeof plain !== "object") return throwError(new Error(`value[${i}] is not an object`));
      const model = plainToClass(modelType, plain, opts.transformer);
      models.push(model);
    }
    return forkJoin(models.map(m => from(validate(m, opts.validator)))).pipe(
      switchMap(errors => {
        for (const e of errors) {
          if (e.length > 0) return throwError(errors);
        }
        return of(models);
      })
    );
  }

  /**
   * Transform a plain object to a model, validate it, and map the result to a plain object.
   *
   * Usage:
   * ```ts
   *  // assuming model is an object adherring to Model interface (only needs to be an instance if model has exposed properties)
   *  modelAdapter.fromModel(Model, model).pipe(
   *    switchMap(plain => httpClient.post(url, plain))
   *  ).subscribe(
   *    () => console.log("200 OK"),
   *    (err: HttpErrorResponse|ValidationError[]) => console.error(err)
   *  );
   * ```
   */
  public fromModel<T>(modelType: new () => T, plain: { [k in keyof T]: T[k] }|T, optionsOverride?: ModelAdapterOptions): Observable<any> {
    const opts = this.overrideOptions(optionsOverride);
    const model = plain instanceof modelType ? plain : plainToClass(modelType, plain, opts.transformer);
    return from(validate(model, opts.validator)).pipe(map(errors => {
      if (errors.length > 0) throw errors;
      return classToPlain(model, opts.transformer) as any;
    }));
  }

  /** Helper method for overriding global options per method */
  private overrideOptions(options?: ModelAdapterOptions): ModelAdapterOptions {
    return options ? {
      validator: options.validator ? { ...this.options.validator, ...options.validator } : this.options.validator,
      transformer: options.transformer ? { ...this.options.transformer, ...options.transformer } : this.options.transformer
    } : this.options;
  }

}
