import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ModelAdapterService } from "ngx-model-adapter";
import { Observable } from "rxjs";
import { Expose } from "class-transformer";
import { IsString, IsUrl } from "class-validator";

class Joke {

  @IsString()
  @Expose({ name: "icon_url" })
  public iconUrl: string;

  @IsString()
  public id: string;

  @IsUrl()
  public url: string;

  @IsString()
  public value: string;

}

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent implements OnInit {
  public joke$: Observable<Joke>;

  constructor(
    private readonly http: HttpClient,
    private readonly modelAdapter: ModelAdapterService
  ) {}

  public ngOnInit() {
    const url = "https://api.chucknorris.io/jokes/random";
    this.joke$ = this.http.get(url).pipe(this.modelAdapter.mapToModel(Joke));
  }
}
